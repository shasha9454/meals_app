import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meal_1_app/mainDrawer.dart';
import 'package:meal_1_app/meas_widget.dart';
import 'package:flutter/material.dart';
import 'package:meal_1_app/catogories.dart';
import 'package:meal_1_app/filterscreen.dart';
import 'package:meal_1_app/Favorites_provider.dart';
import 'package:meal_1_app/filters_provider.dart';

const kfinalresult = {
  Filter.glutenFree: false,
  Filter.lactosefree: false,
  Filter.vegetarian: false,
  Filter.vegan: false,
};

class TabScreen extends ConsumerStatefulWidget {
  const TabScreen({super.key});
  @override
  ConsumerState<TabScreen> createState() {
    return _TabScreenState();
  }
}

class _TabScreenState extends ConsumerState<TabScreen> {
  // void _showalertMessageFavorite(String message) {
  //   ScaffoldMessenger.of(context).clearSnackBars();
  //   ScaffoldMessenger.of(context).showSnackBar(
  //     SnackBar(
  //       content: Text(message),
  //     ),
  //   );
  // }

  void onselect(String identifier) async {
    Navigator.of(context).pop();
    if (identifier == 'filter') {
      await Navigator.of(context)
          .push<Map<Filter, bool>> // -->  Called Generic Method
              (MaterialPageRoute(builder: (ctx) => const FilterScreen()));
      // setState(() {
      // finalList = result ?? kfinalresult;
      // });
    }
  }

  // void _toggleMealFavoriteStatus(Meal meal) {

  //   final isExisting = favoriteList.contains(meal);

  //   if (isExisting == true) {
  //     setState(() {
  //       favoriteList.remove(meal);
  //       _showalertMessageFavorite('Meal is no longer a favorite!');
  //     });
  //   } else {
  //     setState(() {
  //       favoriteList.add(meal);
  //       _showalertMessageFavorite('Marked as a favorite!');
  //     });
  //   }
  // }

  int seindexItems = 0;
  void setIndex(int index) {
    setState(() {
      seindexItems = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    // by using provider ... we are fetching dummy data
    final activeFilter = ref.watch(filtermealsProvider);

    Widget setwidget = Catogories(
      avaliableMeal: activeFilter,
    );
    var activepageTitle = 'Categories';

    if (seindexItems == 1) {
      final favtlist = ref.watch(fList);
      setwidget = MealsWidget(
        title: '',
        list: favtlist,
      );
      activepageTitle = 'favorites';
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(activepageTitle),
      ),
      body: setwidget,
      drawer: MainDrawer(onSelect: onselect),
      bottomNavigationBar: BottomNavigationBar(
        onTap: setIndex,
        currentIndex: seindexItems,
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.set_meal), label: 'Categories'),
          BottomNavigationBarItem(icon: Icon(Icons.star), label: 'faverites'),
        ],
      ),
    );
  }
}
