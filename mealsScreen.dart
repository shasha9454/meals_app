// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:meal_1_app/meal.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:meal_1_app/mealsmeatadata.dart';

class MealsScreen extends StatelessWidget {
  const MealsScreen(
      {super.key, required this.meal, required this.onselectedmeal});
  final Meal meal;
  String get complexity {
    return meal.complexity.name[0].toUpperCase() +
        meal.complexity.name.substring(1);
  }

  String get affordability {
    return meal.affordability.name[0].toUpperCase() +
        meal.affordability.name.substring(1);
  }

  final Function(Meal meal, BuildContext context) onselectedmeal;

  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        elevation: 2,
        margin: const EdgeInsets.all(16),
        clipBehavior: Clip.hardEdge,
        child: InkWell(
          onTap: () {
            onselectedmeal(meal, context);
          },
          child: Stack(
            children: [
              Hero(
                tag: meal.id,
                child: FadeInImage(
                  placeholder: MemoryImage(kTransparentImage),
                  image: NetworkImage(meal.imageUrl),
                  fit: BoxFit.cover,
                  height: 200,
                  width: double.infinity,
                ),
              ),
              Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    color: Colors.black54,
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 50),
                    child: Column(children: [
                      Text(
                        meal.title,
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        softWrap: true,
                        overflow:
                            TextOverflow.ellipsis, // very long text --> ....
                        style:
                            const TextStyle(fontSize: 20, color: Colors.white),
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          MetaScreen(
                            icon: Icons.schedule,
                            label: '${meal.duration}min',
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          MetaScreen(icon: Icons.work, label: complexity),
                          const SizedBox(
                            width: 15,
                          ),
                          MetaScreen(
                              icon: Icons.attach_money, label: affordability)
                        ],
                      )
                    ]),
                  ))
            ],
          ),
        ));
  }
}
