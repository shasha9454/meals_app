import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meal_1_app/filters_provider.dart';

class FilterScreen extends ConsumerStatefulWidget {
  const FilterScreen({super.key});
  @override
  ConsumerState<FilterScreen> createState() {
    return _FilterScreenState();
  }
}

class _FilterScreenState extends ConsumerState<FilterScreen> {
  // var glutenFreeFilterSet = false;
  // var lactoseFreeFilterSet = false;
  // var vegetarianFilterSet = false;
  // var veganFilterSet = false;

  // @override
  // void initState() {
  //   super.initState();
  //   final seletedList = ref.read(FilterProvider);
  //   glutenFreeFilterSet = seletedList[Filter.glutenFree]!;
  //   lactoseFreeFilterSet = seletedList[Filter.lactosefree]!;
  //   veganFilterSet = seletedList[Filter.vegan]!;
  //   vegetarianFilterSet = seletedList[Filter.vegetarian]!;
  // }

  @override
  Widget build(BuildContext context) {
    final list = ref.watch(FilterProvider);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Filters are ...'),
      ),
      // body: PopScope(
      //   canPop: true,
      //   onPopInvoked: (bool didpop) async {
      //     if (didpop) return;
      //     ref.read(FilterProvider.notifier).setFilter({
      //       Filter.glutenFree: glutenFreeFilterSet,
      //       Filter.lactosefree: lactoseFreeFilterSet,
      //       Filter.vegetarian: vegetarianFilterSet,
      //       Filter.vegan: veganFilterSet,
      //     });
      body: Column(
        children: [
          SwitchListTile(
            value: list[Filter.glutenFree]!,
            onChanged: (ischecked) {
              ref
                  .read(FilterProvider.notifier)
                  .setfilters(Filter.glutenFree, ischecked);
            },
            title: Text(
              'Gluten-free-food',
              style: Theme.of(context).textTheme.titleLarge!.copyWith(
                  color: Theme.of(context).colorScheme.onBackground,
                  fontSize: 24),
            ),
            subtitle: Text(
              'Involve following food gluten-free',
              style: Theme.of(context).textTheme.labelMedium!.copyWith(
                  color: Theme.of(context).colorScheme.onBackground,
                  fontSize: 15),
            ),
            activeColor: Theme.of(context).colorScheme.tertiary,
            contentPadding: const EdgeInsets.only(left: 34, right: 24),
          ),
          SwitchListTile(
            value: list[Filter.lactosefree]!,
            onChanged: (onchecking) {
              ref
                  .read(FilterProvider.notifier)
                  .setfilters(Filter.lactosefree, onchecking);
            },
            title: Text(
              'Lactose-free-food',
              style: Theme.of(context).textTheme.titleLarge!.copyWith(
                  color: Theme.of(context).colorScheme.onBackground,
                  fontSize: 24),
            ),
            subtitle: Text(
              'Involve following food lactose-free',
              style: Theme.of(context)
                  .textTheme
                  .labelMedium!
                  .copyWith(color: Theme.of(context).colorScheme.onBackground),
            ),
            activeColor: Theme.of(context).colorScheme.tertiary,
            contentPadding: const EdgeInsets.only(left: 34, right: 24),
          ),
          SwitchListTile(
            value: list[Filter.vegetarian]!,
            onChanged: (onChanged) => ref
                .read(FilterProvider.notifier)
                .setfilters(Filter.vegetarian, onChanged),
            title: Text(
              'Vegetarian-food',
              style: Theme.of(context)
                  .textTheme
                  .titleLarge!
                  .copyWith(color: Theme.of(context).colorScheme.onBackground),
            ),
            subtitle: Text(
              'Involve following food Vegetarian Food ',
              style: Theme.of(context)
                  .textTheme
                  .labelMedium!
                  .copyWith(color: Theme.of(context).colorScheme.onBackground),
            ),
            activeColor: Theme.of(context).colorScheme.tertiary,
            contentPadding: const EdgeInsets.only(left: 34, right: 24),
          ),
          SwitchListTile(
            value: list[Filter.vegan]!,
            onChanged: (onChanged) => ref
                .read(FilterProvider.notifier)
                .setfilters(Filter.vegan, onChanged),
            title: Text(
              'Vegan-food',
              style: Theme.of(context)
                  .textTheme
                  .titleLarge!
                  .copyWith(color: Theme.of(context).colorScheme.onBackground),
            ),
            subtitle: Text(
              'Involve following food Vegan Food ',
              style: Theme.of(context)
                  .textTheme
                  .labelMedium!
                  .copyWith(color: Theme.of(context).colorScheme.onBackground),
            ),
            activeColor: Theme.of(context).colorScheme.tertiary,
            contentPadding: const EdgeInsets.only(left: 34, right: 24),
          ),
        ],
      ),
    );
  }
}
