import 'package:flutter/material.dart';
import 'package:meal_1_app/categories_bar.dart';
import 'package:meal_1_app/category_data.dart';
import 'package:meal_1_app/category_widget.dart';
import 'package:meal_1_app/meal.dart';
import 'package:meal_1_app/meas_widget.dart';

class Catogories extends StatefulWidget {
  const Catogories({super.key, required this.avaliableMeal});

  final List<Meal> avaliableMeal;

  @override
  State<Catogories> createState() => _CatogoriesState();
}

class _CatogoriesState extends State<Catogories>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    _animationController = AnimationController(
        vsync: this,
        duration: const Duration(milliseconds: 300),
        lowerBound: 0,
        upperBound: 1);
    super.initState();

    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void _selectedCategory(BuildContext context, category category) {
    final resultlist = widget.avaliableMeal
        .where((element) => element.categories.contains(category.id))
        .toList();
    Navigator.of(context).push(
      MaterialPageRoute(
          builder: (ctx) => MealsWidget(
                title: category.title,
                list: resultlist,
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      child: GridView(
          padding: const EdgeInsets.all(24),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 20,
            childAspectRatio: 3 / 2,
            mainAxisSpacing: 20,
          ),
          children: [
            for (final category in categories)
              CategoryWidget(
                CategoryData: category,
                onTap: () {
                  _selectedCategory(context, category);
                },
              )
          ]),
      builder: (context, child) =>
          // Padding(
          // padding: EdgeInsets.only(
          //   top: 100 - _animationController.value * 100,
          // ),
          SlideTransition(
        position: Tween(
          begin: const Offset(0, 0.3),
          end: const Offset(0, 0),
        ).animate(
          CurvedAnimation(
              parent: _animationController, curve: Curves.easeInOut),
        ),
        child: child,
      ),
    );
  }
}
