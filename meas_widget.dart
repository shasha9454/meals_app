import 'package:flutter/material.dart';

import 'package:meal_1_app/meal.dart';
import 'package:meal_1_app/mealsScreen.dart';
import 'package:meal_1_app/mealsData.dart';

// ignore: must_be_immutable
class MealsWidget extends StatelessWidget {
  const MealsWidget({
    super.key,
    required this.title,
    required this.list,
  });
  final String? title;
  final List<Meal> list;

  void onselectMethod(Meal meal, BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (ctx) => MealsData(
        meal: meal,
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    Widget content = const Text("No data Present here !");

    if (list.isNotEmpty) {
      content = ListView.builder(
          itemCount: list.length,
          itemBuilder: (ctx, indx) {
            return MealsScreen(
              meal: list[indx],
              onselectedmeal: onselectMethod,
            );
          });
    }
    // ignore: unnecessary_null_comparison
    if (title == null) {
      return content;
    }
    return Scaffold(
        appBar: AppBar(
          title: Text(title!),
        ),
        body: content);
  }
}
