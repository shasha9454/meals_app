import 'package:flutter/material.dart';
import 'package:meal_1_app/categories_bar.dart';

class CategoryWidget extends StatelessWidget {
  // ignore: non_constant_identifier_names
  const CategoryWidget(
      // ignore: non_constant_identifier_names
      {super.key,
      required this.CategoryData,
      required this.onTap});
  // ignore: non_constant_identifier_names
  final category CategoryData;
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(16),
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          gradient: LinearGradient(colors: [
            CategoryData.color.withOpacity(0.55),
            CategoryData.color.withOpacity(0.9)
          ], begin: Alignment.topLeft, end: Alignment.bottomRight),
        ),
        child: Text(
          CategoryData.title,
          style: Theme.of(context)
              .textTheme
              .titleLarge!
              .copyWith(color: Theme.of(context).colorScheme.onBackground),
        ),
      ),
    );
  }
}
