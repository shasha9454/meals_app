import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meal_1_app/meal.dart';

// ignore: camel_case_types
class favoritesListProvider extends StateNotifier<List<Meal>> {
  favoritesListProvider() : super([]);
  // ignore: non_constant_identifier_names
  bool ToggleChangesStatus(Meal meal) {
    final isExit = state.contains(meal);
    if (isExit) {
      state = state.where((element) => element.id != meal.id).toList();
      return false;
    } else {
      state = [...state, meal];
      return true;
      // spread operator fetch listitem one by one separetely and last add meal because here are not supported add or remove method
    }
  }
}

final fList = StateNotifierProvider<favoritesListProvider, List<Meal>>(
    (ref) => favoritesListProvider());
