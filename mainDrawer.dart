// ignore_for_file: file_names

import 'package:flutter/material.dart';

// ignore: use_key_in_widget_constructors
class MainDrawer extends StatelessWidget {
  const MainDrawer({super.key, required this.onSelect});
  final Function(String identifier) onSelect;
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          DrawerHeader(
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Theme.of(context).colorScheme.primaryContainer,
                  Theme.of(context)
                      .colorScheme
                      .primaryContainer
                      .withOpacity(0.6),
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              ),
            ),
            child: Row(
              children: [
                Icon(
                  Icons.restaurant,
                  size: 48,
                  color: Theme.of(context).colorScheme.primary,
                ),
                const SizedBox(
                  width: 28,
                ),
                Text(
                  'Cooking Up!',
                  style: Theme.of(context).textTheme.titleLarge!.copyWith(
                        color: Theme.of(context).colorScheme.primary,
                        fontSize: 24,
                      ),
                ),
              ],
            ),
          ),
          ListTile(
            leading: Icon(
              Icons.fastfood,
              size: 25,
              color: Theme.of(context).colorScheme.primary,
            ),
            title: Text(
              "filters",
              style: Theme.of(context).textTheme.bodySmall!.copyWith(
                  color: Theme.of(context).colorScheme.primary, fontSize: 23),
            ),
            onTap: () {
              onSelect('filter');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.food_bank,
              size: 25,
              color: Theme.of(context).colorScheme.primary,
            ),
            title: Text(
              'Meals',
              style: Theme.of(context).textTheme.bodySmall!.copyWith(
                  color: Theme.of(context).colorScheme.primary, fontSize: 23),
            ),
            onTap: () {
              onSelect('meals');
            },
          )
        ],
      ),
    );
  }
}
