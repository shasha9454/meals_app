import 'package:flutter/material.dart';
import 'package:meal_1_app/categories_bar.dart';

final categories = [
  category(id: 'c1', title: 'Italian', color: Colors.purple),
  category(id: 'c2', title: 'Quick & Easy', color: Colors.red),
  category(id: 'c3', title: 'Hamburger', color: Colors.orange),
  category(id: 'c4', title: 'Asia', color: Colors.pink),
  category(id: 'c5', title: 'Russian', color: Colors.brown),
  category(id: 'c6', title: 'Breakfast', color: Colors.blueGrey),
  category(id: 'c7', title: 'Light & Lovely', color: Colors.blueAccent),
  category(id: 'c8', title: 'Summer', color: Colors.lightGreen),
  category(id: 'c9', title: 'Exotic', color: Colors.green),
  category(id: 'c10', title: 'German', color: Colors.redAccent),
];
