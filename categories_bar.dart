import 'dart:ui';

// ignore: camel_case_types
class category {
  category({required this.id, required this.title, required this.color});
  final String id;
  final String title;
  final Color color;
}
