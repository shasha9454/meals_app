import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meal_1_app/meals_provider.dart';

enum Filter { glutenFree, lactosefree, vegetarian, vegan }

class FilterStatus extends StateNotifier<Map<Filter, bool>> {
  FilterStatus()
      : super({
          Filter.glutenFree: false,
          Filter.lactosefree: false,
          Filter.vegan: false,
          Filter.vegetarian: false,
        });

  void setfilters(Filter filter, bool isactive) {
    state = {...state, filter: isactive};
  }

  void setFilter(Map<Filter, bool> curVal) {
    state = curVal;
  }
}

// ignore: non_constant_identifier_names
final FilterProvider = StateNotifierProvider<FilterStatus, Map<Filter, bool>>(
  (ref) => FilterStatus(),
);
final filtermealsProvider = Provider((ref) {
  final favoritelist = ref.watch(finalMeals);
  final activeFilter = ref.watch(FilterProvider);
  return favoritelist.where((element) {
    if (activeFilter[Filter.glutenFree]! && !element.isGlutenFree) {
      return false;
    }
    if (activeFilter[Filter.lactosefree]! && !element.isLactoseFree) {
      return false;
    }
    if (activeFilter[Filter.vegan]! && !element.isVegan) {
      return false;
    }
    if (activeFilter[Filter.vegetarian]! && !element.isVegetarian) {
      return false;
    }
    return true;
  }).toList();
});
